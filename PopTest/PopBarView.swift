//
//  PopBarView.swift
//  PopTest
//
//  Created by pies on 2017/12/16.
//  Copyright © 2017年 pies. All rights reserved.
//

import UIKit

class PopBarView: UIView {

    var firstX: CGFloat = 0
    
    var firstPoint: CGPoint!
    
    weak var containerV: PopContainerView?
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    public convenience init(frame: CGRect, container: PopContainerView) {
        self.init(frame: frame)
        self.containerV = container
        container.addSubview(self)
        container.bringSubview(toFront: self)
    }

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white .withAlphaComponent(0.0)
    }
    
    override func draw(_ rect: CGRect) {

        let path:UIBezierPath = UIBezierPath();
        path.lineWidth = 2.0
        // 色の設定
        UIColor.red.setStroke()

        // 起点
        path.move(to: CGPoint(x: (rect.width / 2.0 - 1.0),
                                 y: 0.0))
        // 帰着点
        path.addLine(to: CGPoint(x: (rect.width / 2.0 - 1.0),
                                 y: rect.height))
        // ラインを結ぶ
        path.close()
        path.stroke()
    }
    
    func setFirstPoint(_ pt: CGPoint) {
        firstX = pt.x - (self.frame.width / 2.0)
        if firstX < 0 {
            firstX = 0 - (self.frame.width / 2.0)
        }
        self.frame.origin = CGPoint(x: firstX, y: 0)
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        // タッチされたビューを取得
        let v = super.hitTest(point, with: event)
        if v == self {
            print("popline event")
            return self
        } else if v == containerV {
            containerV?.endPop()
        }
        return nil
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("touch begin")
        if let touch = touches.first {
            firstPoint = touch.location(in:self)
        }
    }
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("moved")
        if let touch = touches.first {
            let loc = touch.location(in:containerV)
            
            let x = loc.x - (self.frame.width / 2.0)
            let adjustX = containerV!.adjustDragX(x)
            
            self.frame.origin = CGPoint(x: adjustX, y: 0)
            // pop lineのX座標(左端X座標なので、Line分半分ずらす)
            containerV?.detectPopLineMoved(x: adjustX + (self.frame.width / 2.0))
        }
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("ended")
    }
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("canceled")
    }
    
    
}
