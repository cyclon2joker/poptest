//
//  CellDrawView.swift
//  PopTest
//
//  Created by pies on 2017/12/16.
//  Copyright © 2017年 pies. All rights reserved.
//

import UIKit


class CellDrawView: UIView {
    
    var drawInfos = [[String:Any]]()
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.backgroundColor = UIColor(displayP3Red: 0.5, green: 1, blue: 1, alpha: 0.5).withAlphaComponent(0)
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor(displayP3Red: 0.5, green: 1, blue: 1, alpha: 0.5)
    }

    
    func setDrawPoint(_ infos: [[String:Any]]) {
        drawInfos.removeAll()
        for info in infos {
            drawInfos.append(info)
        }
    }

    
    
    override func draw(_ rect: CGRect) {
        // todo
        
        for info in drawInfos {
            let pt = info["pt"] as! CGPoint
            
//            let centerPt = CGPoint(x: sz.width / 2.0, y: sz.width / 2.0)
            let angle = CGFloat(Double.pi) * 2.0
            let radius: CGFloat = 6.0
            
            UIColor.blue.setStroke()
            
            let path:UIBezierPath = UIBezierPath();
            path.lineWidth = 1
            path.addArc(withCenter: pt, radius: radius,
                        startAngle: 0.0, endAngle: angle, clockwise: true)
            path.stroke()
            path.fill()
            
        }
        
        
        
//        drawInfo.append(["txt": "1:0", "pt": CGPoint(x:100,y:50)])
//        drawInfo.append(["txt": "1:1", "pt": CGPoint(x:200,y:100)])
//        drawInfo.append(["txt": "1:2", "pt": CGPoint(x:300,y:150)])

    }
    
    
    
}
