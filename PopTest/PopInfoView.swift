//
//  PopInfoView.swift
//  PopTest
//
//  Created by pies on 2017/12/16.
//  Copyright © 2017年 pies. All rights reserved.
//

import UIKit

class PopInfoView: UIView {
    
    var desc: String = ""
    var pt: CGPoint!
    var lbl: UILabel!

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white
    }
    public convenience init(frame: CGRect, info: [String: Any]) {
        self.init(frame: frame)
//        self.isHidden = false
//        self.alpha = 1.0

        self.layer.borderWidth = 2.0
        self.layer.borderColor = UIColor.orange.cgColor
        self.layer.cornerRadius = 5.0

        desc = "\(info["txt"] as! String)"
        pt = info["pt"] as! CGPoint

        lbl = UILabel(frame: frame)
        lbl.frame.origin = CGPoint(x: 0, y: 0)
        lbl.numberOfLines = 2
        lbl.text = "\(desc)\n(x:\(Int(pt.x)), y:\(Int(pt.y)))"
        lbl.textColor = UIColor.red
        lbl.adjustsFontSizeToFitWidth = true
        self.addSubview(lbl)
        self.bringSubview(toFront: lbl)
        
        
        let tapEvent = UITapGestureRecognizer(target:self,
                                              action:#selector(tapMe))
        self.addGestureRecognizer(tapEvent)
        
    }

    @objc func tapMe(sender: UITapGestureRecognizer)
    {
        // 自身をタップしたら最前面
        print("pop tap")
        UIView.animate(withDuration: 1.5,
                       delay: 0.0,
                       options: .curveEaseIn,
                       animations: {[unowned self] in
                        self.superview?.bringSubview(toFront: self)
        }, completion: nil)

        
    }

    
    
    
}


