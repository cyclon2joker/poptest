//
//  ViewController.swift
//  PopTest
//
//  Created by pies on 2017/12/16.
//  Copyright © 2017年 pies. All rights reserved.
//

import UIKit

class ViewController: UIViewController,
    UICollectionViewDelegateFlowLayout, UICollectionViewDataSource,
    PopContainerDelegate {

    @IBOutlet weak var tblHoge: UICollectionView!
    
    @IBOutlet weak var containerView: UIView!

    @IBOutlet weak var guardR: UIView!
    
    @IBOutlet weak var guardL: UIView!
    
    
    
    var longPressEvent: UILongPressGestureRecognizer!
    
    var popContainer: PopContainerView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblHoge.dataSource = self
        tblHoge.delegate = self
        
        // dragイベント登録
        longPressEvent = UILongPressGestureRecognizer(target:self,
                                           action:#selector(longPressed))
        
        tblHoge.addGestureRecognizer(longPressEvent)
        
        popContainer = PopContainerView(frame:containerView.frame)
        popContainer.isHidden = true
        popContainer.backgroundColor = UIColor(displayP3Red: 0, green: 0, blue: 0, alpha: 0.5)
        containerView.addSubview(popContainer)
        popContainer.delegate = self
        
        containerView.bringSubview(toFront: guardL)
        containerView.bringSubview(toFront: guardR)
        guardL.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        guardR.backgroundColor = UIColor.black.withAlphaComponent(0.7)


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }

    @IBAction func tapBtnForceHide(_ sender: Any) {
        
        self.endPopLine(popContainer: self.popContainer)
//        popContainer.isHidden = true
//        tblHoge.addGestureRecognizer(longPressEvent)
        
    }
    
    func endPopLine(popContainer: PopContainerView) {
        
        popContainer.isHidden = true
        tblHoge.addGestureRecognizer(longPressEvent)

    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // 4ページ
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.containerView.frame.size
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CELL", for: indexPath)
        
        let lbl: UILabel = cell.viewWithTag(1001) as! UILabel
        
        lbl.text = "\(indexPath.item + 1)"
        if indexPath.item % 2 == 0 {
            cell.backgroundColor = UIColor.lightGray
        } else {
            cell.backgroundColor = UIColor.white
        }
        
        let imgV: CellDrawView = cell.viewWithTag(1002) as! CellDrawView
        
        var drawInfo = [[String:Any]]()

        switch indexPath.item {
        case 0:
            drawInfo.append(["txt": "page1:1", "pt": CGPoint(x:100,y:50)])
            drawInfo.append(["txt": "page1:2", "pt": CGPoint(x:150,y:100)])
            drawInfo.append(["txt": "page1:3", "pt": CGPoint(x:300,y:150)])
        case 1:
            drawInfo.append(["txt": "page2:1", "pt": CGPoint(x:100,y:30)])
            drawInfo.append(["txt": "page2:2", "pt": CGPoint(x:150,y:200)])
            drawInfo.append(["txt": "page2:3", "pt": CGPoint(x:200,y:220)])
        case 2:
            drawInfo.append(["txt": "page3:1", "pt": CGPoint(x:100,y:50)])
            drawInfo.append(["txt": "page3:2", "pt": CGPoint(x:150,y:100)])
            drawInfo.append(["txt": "page3:3", "pt": CGPoint(x:200,y:150)])
        case 3:
            drawInfo.append(["txt": "page4:1", "pt": CGPoint(x:100,y:150)])
            drawInfo.append(["txt": "page4:2", "pt": CGPoint(x:150,y:200)])
            drawInfo.append(["txt": "page4:3", "pt": CGPoint(x:200,y:250)])
        default:
            drawInfo.append(["txt": "pageX:1", "pt": CGPoint(x:100,y:150)])
            drawInfo.append(["txt": "pageX:2", "pt": CGPoint(x:150,y:200)])
            drawInfo.append(["txt": "pageX:3", "pt": CGPoint(x:200,y:250)])
        }
        
        imgV.setDrawPoint(drawInfo)
        imgV.setNeedsDisplay()
        
        cell.bringSubview(toFront: lbl)
        return cell
    }
    @objc func longPressed(sender: UILongPressGestureRecognizer)
    {
        
        print("longpressed")
        tblHoge.removeGestureRecognizer(self.longPressEvent)
        let longTapPoint = sender.location(in: containerView)
        popContainer.frame.origin = CGPoint(x:0,y:0)
        popContainer.showPopLine(longTapPoint)
        
    }
    
    func detectPopLineMoved(popContainer: PopContainerView, x: CGFloat) {
        
        let scrollOffset = tblHoge.contentOffset
        
        for cell in self.tblHoge.visibleCells {
         
            print("@@@idx:\(tblHoge.indexPath(for: cell)!))")
            print("@@@cell[\(tblHoge.indexPath(for: cell)!.item)],\(cell.frame.origin.x),\(cell.frame.origin.y)")
            
            let dv = cell.viewWithTag(1002) as! CellDrawView
            let addFunc = {(info:[String: Any],cellX: CGFloat) -> [String: Any] in
                var wrk = [String: Any]()
                wrk["txt"] = info["txt"]
                let pt = info["pt"] as! CGPoint
                wrk["pt"] = CGPoint(x: pt.x - scrollOffset.x + cellX, y: pt.y)
                return wrk
            }
            let infos = dv.drawInfos.map({addFunc($0, cell.frame.origin.x)})
//            print("@@@AAA:\(dv.drawInfos)")
//            print("@@@BBB:\(infos)")
            
            let hits = infos.filter({
//                x == ($0["pt"] as! CGPoint).x
                (x - 3 <= ($0["pt"] as! CGPoint).x) &&
                    (x + 3 >= ($0["pt"] as! CGPoint).x)
            })

            // 該当座標あり
            if hits.count > 0 {                // popup通知
                popContainer.popInfo(hits[0])
            }
        }
        print("@@@*contentoffset:\(tblHoge.contentOffset)")
        
    }

    
    

}

