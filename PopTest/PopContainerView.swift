//
//  PopContainerView.swift
//  PopTest
//
//  Created by pies on 2017/12/16.
//  Copyright © 2017年 pies. All rights reserved.
//

import UIKit

// イベント通知Delegate
protocol PopContainerDelegate: class {

    func endPopLine(popContainer: PopContainerView)
    func detectPopLineMoved(popContainer: PopContainerView, x: CGFloat)

    
}

class PopContainerView: UIView {

    var popBar: PopBarView!
    var delegate: PopContainerDelegate?
    
    let WIDTH_POPBAR: CGFloat = 44.0
    let DEF_GURAD_MERGIN: CGFloat = 40.0

    var popViews = [NSValue: PopInfoView]()
    
    

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.clipsToBounds = true
        
        let rct = CGRect(x: (frame.size.width - WIDTH_POPBAR) / 2.0,
                         y: 0,
                         width: WIDTH_POPBAR,
                         height: frame.size.height)
        popBar = PopBarView(frame:rct , container: self)
    }

    @objc func tapped(sender: UITapGestureRecognizer)
        //    func longPressed(sender: UILongPressGestureRecognizer)
    {
        print("tapped")
        
    }
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        // タッチされたビューを取得
        let v = super.hitTest(point, with: event)
        if v == popBar {
            // バーはイベント継続
            return popBar
        } else if let popV = v as? PopInfoView {
            // ポップはイベント継続
            return popV
        }
        // その他は、ポップモード終了
        print("popbar end...")
        delegate?.endPopLine(popContainer: self)
        return nil
    }
    
    func showPopLine(_ longTapPoint: CGPoint) {
        superview?.bringSubview(toFront: self)

        //
        let popVs = self.subviews.filter({($0 as? PopInfoView) != nil})
        popVs.forEach({
            $0.removeFromSuperview()
        })
        //
        popViews.removeAll()
        
        self.popBar.setFirstPoint(longTapPoint)
        self.isHidden = false

        // 最初の線の位置
        let firstLinePtX = popBar.frame.origin.x + (popBar.frame.width / 2.0)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [unowned self] in
            self.delegate?.detectPopLineMoved(popContainer: self, x: firstLinePtX)
        }
    }

    func adjustDragX(_ x: CGFloat) -> CGFloat {
        
        if x < DEF_GURAD_MERGIN - (WIDTH_POPBAR  / 2.0){
            print("x:\(x)")
            return DEF_GURAD_MERGIN - (WIDTH_POPBAR  / 2.0)
        }
        else if x > self.frame.size.width - DEF_GURAD_MERGIN - (WIDTH_POPBAR / 2.0)  {
            
            print("x2:\(x)")
            return self.frame.size.width - DEF_GURAD_MERGIN - (WIDTH_POPBAR  / 2.0)
        }
        return x
    }
    
    func endPop() {
        delegate?.endPopLine(popContainer: self)
        
    }
    
    func detectPopLineMoved(x: CGFloat) {
        self.delegate?.detectPopLineMoved(popContainer: self, x: x)
    }

    func popInfo(_ info: [String: Any]) {
        let pt = info["pt"] as! CGPoint
        let ptKey = NSValue(cgPoint: pt)
        
        if let v = popViews[ptKey] {
            v.superview?.bringSubview(toFront: v)
        } else {
            let rct = CGRect(x: pt.x + 10.0,
                             y: pt.y - 44.0 - 10.0,
                             width: 135.0,
                             height: 44.0)
            let v = PopInfoView(frame:rct, info: info)
            popViews[ptKey] = v
            self.addSubview(v)
//            v.isHidden = true
            v.alpha = 0.0
            self.bringSubview(toFront: v)
            UIView.animate(withDuration: 1.5,
                           delay: 0.0,
                           options: .curveEaseIn,
                           animations: {
                            v.isHidden = false
                            v.alpha = 1.0
            }, completion: nil)
            
//            UIView.animate(withDuration: 500, animations: {
//            })
            
        }
        
    }
    
    

}
